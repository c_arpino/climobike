### ClimoBike é uma central de monitoramento portátil da qualidade do ar e das condições climáticas, adaptável a bicicletas urbanas.

![Climobike](https://tecnologias.libres.cc/c_arpino/climobike/raw/master/climobike-v1/Figuras/projeto-climobike-recorte.jpg)

[Foto do projeto no festival de tecnologias red bull basement]

## Contatos
### **Climobike (Versão 1):**
- Idealizador e gestor do projeto:
    - *Saulo Machado de Souza Jacques* - saulojacques@protonmail.com

### **Climobike (Versão 2):**
- Gestores do projeto:
    - *Alisson Claudino de Jesus* - alissom.claudino@ufrgs.br
    - *Cristthian Marafigo Arpino* - cristthian.m.arpino@protonmail.com
 
- Desenvolvedor:
    - *Iuri Guilherme dos Santos Martins* - iuri@protonmail.com
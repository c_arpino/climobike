## Avisos legais e licenciamento

Primeiramente pensamos em escrever um desses longos termos e condições aqui. O que é super chato de ler e escrever. Então para facilitar, fizemos uma versão curta: Tudo é gratuito para qualquer uso (comercial ou pessoal). Nós apreciaríamos se você mencionasse nosso nome em algum lugar. Ah, e você não pode nos processar se algo der errado. Por isso seja cuidadoso! Mas para aqueles que gostam de versões longas de termos e confições, podem conferir abaixo as licenças que utilizamos:

- **CERN Open Hardware License v1.2**
    - Esta documentação descreve projetos de hardware aberto e livre. Os projetos de hardware estão licenciados sob termos da licença de hardware aberto do CERN OHL v.1.2. O que permite a liberdade do conhecimento para que qualquer um possa usar, estudar, modificar, distribuir, redistribuir e vender este projeto livremente, contanto que sigam os termos e condições da licença, como por exemplo, é vetada a possíbilidade de patentear o projeto ou projetos similares a este, saiba mais no texto da versão completa da licença acesse [CERN Open Hardware License v1.2](https://www.ohwr.org/projects/cernohl/wiki)
    - ![CERN OHL v1.2](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CERN_OHL_v1.2.76x80.png)
- **GPL GNU v3**
    - E os softwares desenvolvidos neste projeto para aplicações no Anemômetro e medições do potêncial eólico são disponibilizados sob a licença [GPL GNU](https://www.gnu.org/licenses/gpl-3.0.html) versão 3.
    - ![GPL 3.0](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/GPL_GNU_v3.88x35.png)
- **Attribution ShareAlike [(CC BY-SA 4.0 Internacional)](https://creativecommons.org/licenses/by-sa/4.0/legalcode.pt)**
    - Esta obra está licenciado com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional.
    - ![CC BY-SA](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CC_BY-SA.88x31.png)

Autores: Saulo Machado de Souza Jacques, Cristthian Marafigo Arpino, Alisson Claudino de Jesus e Iuri Martins.
